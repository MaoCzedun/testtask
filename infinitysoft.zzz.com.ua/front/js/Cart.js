// разрешить вставку
var cookieEnabled  = navigator.cookieEnabled;
window.onload = function(){
    var cartData=null;
    if(typeof localStorage['cartData']!=='undefined')
    {
        cartData = JSON.parse(localStorage['cartData']);  
    } 
    if(cartData)
    {

        $('#cartData > div').html("\<span>Number of gods:"+cartData.numOfGods+"<\/span><br><span>Cost:"+ cartData.summary +"<\/span>");
        renderProducts(cartData);     
    }
    if(document.cookie.indexOf('cartData')===-1 &&typeof localStorage['cartDataCookie']!=='undefined')
    {
       var date = new Date(new Date().getTime() + 60 * 1000);
       console.log('test');
       document.cookie = "cartData="+localStorage['cartDataCookie']+ ";path=/; expires=" + date.toUTCString();
    }
}
$('#addProduct,.products > .product > span > a.btn.btn-default').on('click',function(event){
    event.preventDefault();
    
    $.get(
        this.href,
        null,
        function(data){
            setCartData(data); 
        });
});
$('#clearCart').on('click',function(event){
   $.get('/cart/clearcart',null,function(){
        localStorage.removeItem('cartData');
        $('#cartData > div ').html("<span>Number of gods:0</span><br><span>Cost: 0.00 $</span>");
   }) 
});
 $(document).ready(function() {
    $('#cartData').tooltipster({trigger:'hover'});
    $('#cartData > a').magnificPopup({type:'inline',preloader: false,showCloseBtn:false}) 
});
function renderProducts(cartData)
{
    var html='',htmlPopup='<div>';
    $.each(cartData,function(key,value){
        if(/\d+/.test(key))
        {
            html += "<span>"+value.name+" </span><strong>"+value.price+" $</strong><br>";
            htmlPopup += "<span>"+value.name+" </span><strong>"+value.price+" $</strong> \
                        <input readonly=\"readonly\" type=\"text\" value=\" "+value.count+" \" />\n\
                        <input name=\"chang"+key+"\" type=\"number\" />\n\
                        <a  role=\"button\"  data-link=\"chang"+key+"\"  class=\"glyphicon glyphicon-plus\"  title=\"Add product\"    href=\"/cart/addproduct/"+key+"\"></a> \n\
                        <a  role=\"button\"  data-link=\"chang"+key+"\"  class=\"glyphicon glyphicon-minus\" title=\"Delete product\" href=\"/cart/deleteproduct/"+key+"\"></a> \n\
                        <a  role=\"button\"  data-link=\"chang"+key+"\"  class=\"glyphicon glyphicon-trash\" title=\"Remove product\" href=\"/cart/removeproduct/"+key+"\"></a> \n\
                    <br>";
        }
    });
    htmlPopup +='</div>';
//    $('#tooltipCart').html(html);
    $('#cartData').tooltipster('content', $(html));
    $('#cartPopup').html(htmlPopup);
    $('#cartPopup > div > a').on('click',function(event){
        event.preventDefault();
        var count = $('input[name="'+this.dataset.link+'"]').val();
        count = count!==''  ? count : null;
        $.get(this.href,{count:count},function(data){
            setCartData(data);
        });

    });    
    $('#cartPopup > div > input[type="number"]').on('keydown',function(e){          
        var eKey = e.which || e.keyCode,arr = [8,46,37,39,220];
        return /\d/.test(String.fromCharCode(eKey)) || arr.indexOf(eKey)!==-1;
    });
    $('#cartPopup > div > a').tooltipster({trigger:'hover'});

}
function setCartData(data)
{
    localStorage['cartData'] = data;
    localStorage['cartDataCookie'] = encodeURIComponent($.cookie('cartData')); 
    var cartData = JSON.parse(data);
    $('#cartData > div').html("\<span>Number of gods:"+cartData.numOfGods+"<\/span><br><span>Cost:"+ cartData.summary +"<\/span>");
    renderProducts(cartData);
    
}