<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Router
 *
 * @author Дом
 */
// реализовать метод  создания URL
require_once 'Controller.php';
require_once 'Model.php';
class Router
{
    
    private $routes;
    public function __construct()
    {
        $this->routes= require_once CONFIG.'routes.php';
    }
    public function getRoute($route)
    {
        if(!in_array($route,$this->routes))
        {
            return $routes['Error/404'];
        }
        file_exists($filename);
    }
    public function redirect($url,$replace=true,$statusCode=302)
    {
        header("Location:{$url}",$replace,$statusCode);
    }
    public function manage(&$dataBase)
    {
        
        $url = $_SERVER['REQUEST_URI'];
        $regexParams = "/\?.+$/";
        $url =  preg_replace($regexParams,'', $url);
        $explodedRoute = explode('/',$url);        
        $controllerName = $explodedRoute[1]==='' ?  'index' :  $explodedRoute[1];
        $methodName = isset($explodedRoute[2]) ? $explodedRoute[2] : 'index';
        $id = isset($explodedRoute[3]) ? $explodedRoute[3] : null;
        $key = $controllerName.'/'.$methodName;
        if(!key_exists($key, $this->routes) || !isset($this->routes[$key]))
        {
            file_put_contents('routerLogs.txt',date('Y.m.d \i\n H:i:s')."Key: {$key} does not exists  in routes.php".PHP_EOL,FILE_APPEND); 
            $this->redirect('/error/404',true,404);
        }
        $controllerName = $this->routes[$key]['controller'];
        $controllerName .='Controller';
        if(file_exists(CONTROLLERS.$controllerName.'.php'))
        {
            require_once CONTROLLERS.$controllerName.'.php';            
        }
        else
        {
            $this->redirect('/error/404',true,200);
        }
        
        $controller =  new $controllerName($dataBase);
        $controller->setLayout();
        if(isset($this->routes[$key]['viewFile']))
        {
            if(!$controller->setViewFile($this->routes[$key]['viewFile']))
            {
                $this->redirect('/error/404',true,200);
            }
        }
        $methodName = $this->routes[$key]['action'];
        if(method_exists($controller, $methodName))
        {
            if($id)
            {
                echo $controller->$methodName($id);
            }
            else
            {
                echo $controller->$methodName();
            }
        }
        else
        {
            file_put_contents('routerLogs.txt',date('Y.m.d \i\n H:i:s')."Method: {$methodName} does not exists  in {$controller}.php".PHP_EOL,FILE_APPEND); 
            $this->redirect('/error/404',true,200);
        }
    }
}
