<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Controller
 *
 * @author Дом
 */
abstract class Controller
{
    protected $content;
    protected $viewFile;
    protected $layout;
    protected $dataStorage=[];
    protected $dataBase;
    protected $returnData;
    protected $jsScripts=[
        'css'=>[],
        'js'=>[]
    ];
    public function __construct(&$dataBase)
    {
        $this->dataBase = $dataBase;        
    }
    final public function checkPermission()
    {
        
    }
    final public function setLayout($layoutFile='default')
    {
        $layoutPath = VIEWS.'layouts'.DIRECTORY_SEPARATOR.$layoutFile.'.php';
        if(!file_exists($layoutPath))
        {
            trigger_error("Layout file {$layoutFile} does not Exists");
            return false;
        }
        else 
        {
            $this->layout = $layoutPath;
            return true;
        }
    }
    final protected function loadModel($modelFile)
    {
        $modelPath = MODELS.$modelFile.'.php';
        if(!file_exists($modelPath))
        {
            trigger_error("Model file {$modelFile} does not Exists");
            return false;
        }
        else
        {
            require_once $modelPath;
            return true;
        }
    }
    final public function setViewFile($viewFile)
    {
        $viewPath = VIEWS.$viewFile.'.php';
        if(!file_exists($viewPath))
        {
            trigger_error("View file {$viewFile} does not Exists");
            return false;
        }
        else 
        {
            $this->viewFile = $viewPath;
            return true;
        }
    }
    final public function viewTemplate()
    {
        extract($this->dataStorage);
        ob_start();
        require_once $this->viewFile;
        $this->content = ob_get_contents();
        ob_end_clean();
        return $this->content;
    }
    final public function returnJSON()
    {
        
        header("Content-Type: application/json;charset=utf-8");
        return json_encode($this->returnData);
    }
    final public function view()
    {
        extract($this->dataStorage);
        ob_start();
        require_once $this->layout;
        $this->content = ob_get_contents();
        ob_end_clean();
        return $this->content;
    }
}
