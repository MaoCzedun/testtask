<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Model
 *
 * @author Дом
 */
abstract class Model
{
   protected $tableName;
   protected $dataBase;
   public $attributes = [];
   public $errors = [];
   
   public function __construct(&$dataBase,$tableName)
   {
       $this->tableName = $tableName;
       $this->dataBase = $dataBase;
   }
   abstract public function validation();
   
   abstract public function saveToDb();
   abstract public function setAttributes(&$attributes);
   public function getAll()
   {
       return $this->dataBase->query("SELECT * FROM `{$this->tableName}`")->fetchAll();
   }
   final public function getById($id)
   {
       return $this->dataBase->query("SELECT * FROM `{$this->tableName}` WHERE `id`={$id}")->fetchObject();
   }
   public function setTableName($tableName)
   {
       $this->tableName = $tableName;
   }
}
