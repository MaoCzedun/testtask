<?php
class Order extends Model
{
    private $cartData;
    
    public function __construct(&$dataBase, $tableName='orders')
    {
        parent::__construct($dataBase, $tableName);
        $this->attributes['email'] = $this->attributes['shipmentId'] = $this->attributes['customerAddress'] = '';
    }

    public function saveToDb()
    {
        $cashBalance = floatval($_COOKIE['userCash']) - $this->cartData['summary'];
//        dump($cashBalance);
        $_COOKIE['userCash'] = $cashBalance;
        
        $this->dataBase->prepare("INSERT INTO `{$this->tableName}`(`email`,`summary`,`shipment_id`,`product_quantity`)  values ('{$this->attributes['email']}',{$this->cartData['summary']},{$this->attributes['shipmentId']},{$this->cartData['numOfGods']});")->execute();
        
        $sqlStatement = "INSERT INTO `orders_products` (`product_id`,`order_id`,`quantity`,`total`) values ";
        $orderId = $this->dataBase->lastInsertId();
        unset($this->cartData['numOfGods'],$this->cartData['summary']);
        $values = [];
        foreach($this->cartData as $id=>$data)
        {
            $values[]= "({$id},{$orderId},{$data['count']},{$data['total']})";
        }
        $sqlStatement .=implode(",", $values).";";
        $this->dataBase->query($sqlStatement);
//        setcookie('cartData','', time() -3600,'/' );
        setcookie('userCash', $cashBalance,time() + (3600*24*365),'/' );
        setcookie( "cartData" ,'',  time() -3600,'/' );
        unset($_COOKIE['cartData']);
    }

    public function validation()
    {
        
        if(isset($this->attributes['email']))
        {
            if(!filter_var($this->attributes['email'],FILTER_VALIDATE_EMAIL))
            {
                $this->errors['email']='email not validated';
            }
        }
        else {
            $this->errors['email']='email empty';
        }
        if(!$this->checkGoogleCaptcha()){
            $this->errors['googleCaptcha'] = 'captcha  failure';
        }        
        if(empty($this->attributes['shipmentId']))
        {
            $this->errors['shipmentId'] = 'shipment not select';
        }
        if(empty($this->attributes['shipmentId']))
        {
            $this->errors['shipmentId'] = 'shipment not select';
        }
        else
        {
            $statement = $this->dataBase->prepare("SELECT `price` FROM shipments where id= {$this->attributes['shipmentId']}  ");
            $statement->execute();
            $shipmentPrice  = floatval($statement->fetchColumn());
            $this->cartData['summary']+=$shipmentPrice;
        }
        if(empty($this->attributes['customerAddress']))
        {
            $this->errors['Address'] = 'Address not set';
        }
        
        if(floatval($_COOKIE['userCash'])<$this->cartData['summary'])
        {
            $this->errors['Cash'] = 'order amount more than your cash';
        }
    }
    public function setCartData($cartData)
    {
        $this->cartData = &$cartData;
    }
    private function checkGoogleCaptcha()
    {
        $url="https://www.google.com/recaptcha/api/siteverify?secret=6LcmkAoUAAAAAOWQ_6hXtjuCEgjnziVCXkbLpTZ-&response={$this->attributes['captchaResponse']}&remoteip={$this->attributes['userIp']}";
        $success = json_decode(file_get_contents($url));
        return $success->success;
    }

    public function setAttributes(&$attributes)
    {
        
        $this->attributes['email'] = $attributes['Order']['customerEmail'];
        $this->attributes['shipmentId'] = $attributes['Order']['shipmentId'];
        $this->attributes['customerAddress'] = strip_tags($attributes['Order']['customerAddress']);
        $this->attributes['captchaResponse'] =$attributes['g-recaptcha-response'];
        $this->attributes['userIp'] = $_SERVER['REMOTE_ADDR'];
        
    }
}
