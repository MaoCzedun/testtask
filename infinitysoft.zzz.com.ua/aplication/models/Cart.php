<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Cart
 *
 * @author Дом
 */
class Cart
{
    private $productsStorage = [];
    public function addToStorage(&$productObject,$count=null)
    {
        if (!empty($this->productsStorage[$productObject->id])) {
            $this->productsStorage[$productObject->id]['count'] += $count ?  $count : 1 ;
        }
        else {
            $this->productsStorage[$productObject->id]=[];
            $this->productsStorage[$productObject->id]['price']=$productObject->price;
            $this->productsStorage[$productObject->id]['name']=$productObject->name;
            $this->productsStorage[$productObject->id]['count']= $count ? $count : 1;
        }
    }
    public function setCount($productId,$count)
    {
        $this->productsStorage[$productId]['count']=$count;
    }
    public function deleteFromStorage($productId,$count)
    {
        $this->productsStorage[$productId]['count']-=$count;;
    }
    public function removeFromStorage($productId)
    {
        unset($this->productsStorage[$productId]);
    }
    public function calculteCart()
    {
        $this->productsStorage['summary'] = 0;
        $this->productsStorage['numOfGods'] = 0;
        $total = 0;
        
        foreach ($this->productsStorage as $productId => &$values)
        {
            if(is_array($values)){
                $total = $values['count']*$values['price'];
                $values['total'] = $total;
                $this->productsStorage['summary'] +=  $total;
                $this->productsStorage['numOfGods'] += $values['count'];
            }   
        }
    }
    public function getCartData()
    {
        $this->calculteCart();
        return $this->productsStorage;
    }
   
}
