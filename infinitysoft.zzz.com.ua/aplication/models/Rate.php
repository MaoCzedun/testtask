<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Rate
 *
 * @author Дом
 */
class Rate extends Model
{
    public $added = [];
    public function __construct(&$dataBase, $tableName='ratings')
    {
        parent::__construct($dataBase, $tableName);
    }

    public function saveToDb()
    {
        $date = new DateTime();
        $this->dataBase->prepare("INSERT INTO `{$this->tableName}`(`rate`,`date`,`product_id`)  values ({$this->attributes['rate']},'{$this->attributes['date']}',{$this->attributes['productId']});")->execute();
        $this->dataBase->prepare("UPDATE products set avg_rating = (SELECT avg(rate) FROM ratings where product_id = {$this->attributes['productId']}) where id = {$this->attributes['productId']} ")->execute();
        $this->added[$this->attributes['productId']] = $this->attributes['rate'];
    }

    public function setAttributes(&$attributes)
    {
        $this->attributes['productId']=$attributes['productId'];
        $this->attributes['rate']=$attributes['rate'];
        $this->attributes['date']=date('Y-m-d H:i:s');
    }

    public function validation()
    {
        
    }
//put your code here
}
