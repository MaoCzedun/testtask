<form action="/order/create" method="post" role="form">
    <div class="form-group">
        <label  for="customerEmail" >Your email</label>
        <input id="customerEmail" type="email" value="<?=$order->attributes['email']?>" name="Order[customerEmail]" class="form-control" placeholder="email"/> 
    </div>
    <div class="form-group">
        <label for="customerAddress">Your address</label>
        <textarea id="customerAddress" value="<?=$order->attributes['customerAddress']?>" class="form-control" name="Order[customerAddress]"></textarea>
    </div>
    <div class="form-group">
        <label for="shipmentIdSelect">Select transport</label>
        <select required="required" id="shipmentIdSelect" class="form-control" name="Order[shipmentId]">
            <option value="-"  selected></option>
        <?php foreach ($shipments as &$value):?>
            <option data-price="<?=$value->price?>" value="<?=$value->id?>" ><?=$value->name?></option>
        <?php endforeach;?>
        </select>
    </div>
    
    <div id="products" class="form-group">
        <?php foreach($cartData as $id=>$data):?>
        <?php  if(is_array($data)): ?>    
        <span>Name:<?=$data['name']?></span> 
        <strong>Price:<?=$data['price']?> $</strong> 
        Count:<input data-order-id="<?=$id?>"  type="number" name="Order[products][<?=$id?>][count]"  value="<?=$data['count']?>" />
        <strong>Summary: <?=$data['total']?> $</strong> <br>
        <?php endif;?>
        <?php endforeach;?>
    </div>
    <div class="form-group">
        <label for="summary">Summary</label>
        <input id="summary" class="form-control" type="text" readonly="readonly" value="<?=$cartData['summary']?>" />
    </div>
    <div class="form-group">
        <div class="g-recaptcha" data-sitekey="6LcmkAoUAAAAAC5L4N9nJEDGdlahE72LluqlPF8w"></div>
    </div>
    <div class="form-group">
        <?php if(isset($failure)):?>
            <input type="submit" class="btn btn-danger" value="Create order"/>
        <?php else: ?>
            <input type="submit" class="btn btn-default" value="Create order"/>
        <?php endif;?>
        
    </div>
</form>
<script type="text/javascript">
$('form[action="/order/create"] > div#products >  input[type="number"]').on('keydown',function(e){          
    var eKey = e.which || e.keyCode,arr = [8,46,37,39,220];
    return /\d/.test(String.fromCharCode(eKey)) || arr.indexOf(eKey)!==-1;
});
$('form[action="/order/create"] > div#products > input').on('change',function(event){
    var form = this;
    $.get(
        '/cart/setcount/'+this.dataset.orderId,
        {count:this.value},
        function(data){
            var cartData = JSON.parse(data);
            $('#summary').val(cartData.summary);
            setCartData(data);
            $(form).next().text('Summary: '+cartData[form.dataset.orderId].total+' $');
    });
});
$('form[action="/order/create"]').on('submit',function(event){
    var $selectVal = $('form[action="/order/create"] > div.form-group > select').val();
    if($selectVal==='-')
    {
        $('div#messages').append('<div id ="alertdiv" class="alert alert-danger">Please select a  transport type.</div>');
        document.location.href = "#messages";
        setTimeout(function() { 
            $("#alertdiv").remove();
        }, 3000);
        return false;
    }      
});
$('form[action="/order/create"] > div.form-group > select ').on('change',function(event){
    
    var price = this.options[this.selectedIndex].dataset.price,
       $summary = $('#summary'),
       summaryVal = <?=$cartData['summary']?>;
    $summary.val(eval(summaryVal+'+'+price)) ;   
});
</script>