<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8 products">
    <?php  foreach ($products as &$product):?>
        <div class="product row">
            <input class="rating" type="hidden" data-start="1" data-stop="6"  data-product-id="<?=$product->id?>" <?php if(key_exists($product->id,$added)):?>value="<?=$added[$product->id]?>"  disabled="disabled"<?php else:?>value="<?=intval($product->avg_rating)?>" <?php endif;?> />
            <span><?=$product->name?></span>
            <span><?=$product->price?>$</span>
            <span><a role="button" class="btn btn-default" href="/cart/addproduct/<?=$product->id?>">Add To Cart</a></span>
        </div>
    <?php endforeach;?> 
    </div>
    <div class="col-md-2"></div>
</div>
<script type="text/javascript">

$('div.products > .product >input').on('change', function () {
   
//   productsDisabled.push(this.dataset.productId);
    
  this.setAttribute('disabled','disabled');
  $.get('/rate/create',{productId:this.dataset.productId,rate:this.value},function(data){
      
  });
});
</script>
