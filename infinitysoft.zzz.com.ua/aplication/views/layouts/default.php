<!-- this is  controller  -->
<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" >
        <link rel="stylesheet" href="/front/styles/style.css" >
        <link rel="stylesheet" href="/front/styles/tooltipster.bundle.min.css" >
        <?php foreach ($this->jsScripts['css'] as $script=>$src):  ?>
        <link rel="stylesheet" href="<?=$src?>" >
        <?php endforeach;?>
        <!-- required scripts  -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>        
        
        
        <script src="/front/js/plugins/tooltipster/dist/js/tooltipster.bundle.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script> 
        <?php foreach ($this->jsScripts['js'] as $script=>$src):  ?>
        <script src='<?=$src?>'></script>
        <?php endforeach;?>       
        
        <title><?=$title?></title>
    </head>
    <body>
        <!-- скрпит загрузки корзины из localStorage -->
        <div class="row jumbotron">
            <div class="col-md-3">
                <h1><?=$title?></h1>
            </div>
            <div class="col-md-9">
                <div class="row cart">
                    <div   id="userCash" class="col-md-4">
                        <img src="/front/img/icons/cash.png" />
                        <span>Your cash:</span><strong><?php if(isset($_COOKIE['userCash'])):?><?=$_COOKIE['userCash']?><?php else: ?>0.00<?php endif;?> $</strong>
                    </div>
                    <div id="cartData"  data-tooltip-content="#tooltipCart" class="col-md-4">
                        <div>
                            <span>Number of gods:0</span><br>
                            <span>Cost: 0.00 $</span>
                        </div>
                        <a  href="#cartPopup">More</a>
                    </div>
                    <div class="col-md-4">
                        <img src="/front/img/icons/1477437770_Cart.png" />
                        <button id="clearCart" class="btn btn-default">Clear Cart</button>
                        <a href="/order/create" role="button" class="btn btn-default">Create Order</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="tooltip-templates">
            <div id="tooltipCart">
                
            </div>
           
        </div>
        <style>
            .white-popup {
                position: relative;
                background: #FFF;
                padding: 20px;
                width: auto;
                max-width: 500px;
                margin: 20px auto;
              }
        </style>
            <div class="mfp-hide" id="cartPopup">
                
            </div>
            <div id="messages">
                <?php if(isset($messages)&&!empty($messages)):?>
                    <?php
                    $types =  [
                        'error'=>'alert-danger',
                        'success'=>'alert-success',
                        'warning'=>'alert-warning',                        
                    ];
                    
                    foreach($messages as $type=>$messages):
                        foreach($messages as $key=>$message):
                    ?>
                        <div class="alert <?=$types[$type]?>">
                            <a href="#" class="close" data-dismiss="alert">×</a>
                            <?=$key?>:<?=$message?>.
                        </div>
                        <?php endforeach;?>
                    <?php endforeach;?>
                <?php endif;?>
            </div>
            <?php  if(isset($this->viewFile)) { require_once $this->viewFile; }   ?>
        <footer class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="col-md-8">    
                    </div>
                    <div class="col-md-4">
                      <p class="muted pull-right">© 2016 Alex Petrov. All rights reserved</p>
                    </div>
                </div>
            </div>
            
        </footer>
        <script type="text/javascript" src="/front/js/Cart.js" ></script>
    </body>
</html>

