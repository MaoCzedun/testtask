<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of OrderController
 *
 * @author Дом
 */
class OrderController extends Controller
{
    
    public function actionCreate()
    {
        $this->jsScripts['js']['googleCaptcha']='https://www.google.com/recaptcha/api.js';
        if(!isset($_COOKIE['cartData']))
        {
            $this->dataStorage['title'] = 'Empty Cart';
            $this->dataStorage['messages']['warning'] = ['Warning'=>'cart is empty'];
            return $this->actionCleanCart();
        }
        $this->loadModel('Order');
        $this->loadModel('Cart');
        $this->loadModel('Shipment');
        $shipment = new Shipment($this->dataBase);
        $this->dataStorage['cartData'] = unserialize($_COOKIE['cartData'])->getCartData();
        $this->dataStorage['title'] = 'Create Order';
        $this->dataStorage['shipments'] = $shipment->getAll();
        $order = new Order($this->dataBase);
        $this->dataStorage['order'] = &$order;
        if(isset($_POST['Order']) && !empty($_POST['Order']))
        {
            
            $order->setAttributes($_POST);
            $order->setCartData($this->dataStorage['cartData']);
            $order->validation();
            if(empty($order->errors))
            {
                $order->saveToDb();
                $this->dataStorage['title'] = 'Success Order';
                $this->dataStorage['messages']['success'] = ['Success'=>'order success create'];
                return $this->actionCleanCart();
            }
            else 
            {
                $this->dataStorage['failure'] = true;
                $this->dataStorage['messages'] = [];
                $this->dataStorage['messages']['error'] = &$order->errors;
            }
        }
        
        
        return $this->view();
        
    }
    private function actionCleanCart()
    {
        header("refresh:5;url=/");
        $this->setViewFile('order'.DIRECTORY_SEPARATOR.'success');       
        return $this->view();
        
    }
}
