<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of IndexController
 *
 * @author Дом
 */
class IndexController extends Controller
{
    public function actionHome()
    {
        
    }
    
    public function actionIndex()
    {
        $this->loadModel('Product');
        $this->loadModel('Rate');
        $this->dataStorage['added']=  isset($_COOKIE['added']) ? unserialize($_COOKIE['added']) : [];
        $this->jsScripts['js']['rating']='/front/js/plugins/bootstrap-rating.min.js';
        $this->jsScripts['css']['rating']='/front/styles/bootstrap-rating.css';
        $product = new Product($this->dataBase,'products');
        $this->dataStorage['products'] = $product->getAll();
        $this->dataStorage['title'] = 'Main Page';
        $this->dataStorage['welcome'] = 'Welcome to our shop';
        
//        dump($this->dataStorage['products']);
        if(!isset($_COOKIE['userCash']))
        {
            $_COOKIE['userCash'] = 100;
            setcookie('userCash', 100,time() + (3600*24*365),'/' );
        }
//        dump($this->dataStorage);
        return $this->view();
        
    }
}
