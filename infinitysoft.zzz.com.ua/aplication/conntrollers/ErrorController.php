<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ErrorConroller
 *
 * @author Дом
 */
class ErrorController extends Controller
{
    public function action404()
    {
        $this->dataStorage['title'] = 'Not Found';
        $this->dataStorage['errorCode'] = '404';
        return $this->view();
    }
    public function action403()
    {
        $this->dataStorage['title'] = 'Forbidden';
        $this->dataStorage['errorCode'] = '403';
        return $this->view();
    }
    public function action500()
    {
        
        $this->dataStorage['title'] = 'Internal Server Error';
        $this->dataStorage['errorCode'] = '500';
        return $this->view();
    }
}
