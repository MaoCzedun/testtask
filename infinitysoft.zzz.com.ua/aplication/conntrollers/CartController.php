<?php
class CartController  extends Controller
{
    private $cart;
    public function __construct(&$dataBase)
    {
        parent::__construct($dataBase);
        $this->loadModel('Cart');
        
        $this->cart = (isset($_COOKIE['cartData']))   ? unserialize($_COOKIE['cartData']) : new Cart();
    }

    public function  actionAddProduct($productId=null)
    {        
        $this->loadModel('Product');
        $product = new Product($this->dataBase);
        $count = isset($_GET['count']) ?  intval($_GET['count']) : null;
//        echo $count;
        if($productObject = $product->getById($productId))
        {
            $this->cart->addToStorage($productObject,$count);
            $this->updateCart();
            echo json_encode($this->returnData);
        }
    }
    public function actionSetCount($productId)
    {
        $count = isset($_GET['count']) ?  intval($_GET['count']) : 0;
        $this->cart->setCount($productId,$count);
        $this->updateCart();
        echo json_encode($this->returnData);
    }        
    

    public function actionDeleteProduct($productId)
    {
        $count = isset($_GET['count']) ?  intval($_GET['count']) : 1;
        $this->cart->deleteFromStorage($productId,$count);
        $this->updateCart();
        echo json_encode($this->returnData);
    }
    public function actionRemoveProduct($productId)
    {
        $this->cart->removeFromStorage($productId);
        $this->updateCart();
        echo json_encode($this->returnData);
    }
    public function actionClearCart()
    {
        setcookie( "cartData" ,'',  time() -3600,'/' );
        unset($_COOKIE['cartData']);
    }
    private function updateCart()
    {
        $this->returnData =$this->cart->getCartData();
        $this->returnData['serialized'] = serialize($this->cart);
        setcookie( "cartData" ,$this->returnData['serialized'], time() + (3600*24*5),'/');
    }
    
}
