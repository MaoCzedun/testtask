<?php
define('DEBUG',  in_array($_SERVER['REMOTE_ADDR'], array('127.0.0.1','178.150.241.215')));
define('APP_FOLDER',  getcwd().DIRECTORY_SEPARATOR);
define('CONFIG',  APP_FOLDER.'config'.DIRECTORY_SEPARATOR);
define('CORE',  APP_FOLDER.'aplication'.DIRECTORY_SEPARATOR.'core'.DIRECTORY_SEPARATOR);
define('CONTROLLERS',  APP_FOLDER.'aplication'.DIRECTORY_SEPARATOR.'conntrollers'.DIRECTORY_SEPARATOR);
define('VIEWS',  APP_FOLDER.'aplication'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR);
define('MODELS',  APP_FOLDER.'aplication'.DIRECTORY_SEPARATOR.'models'.DIRECTORY_SEPARATOR);



function errorHandler($errno, $errstr, $errfile, $errline)
{   
    $message = date('Y.m.d \i\n H:i:s')." Error Type:{$errno} Message:{$errstr} File:{$errfile} Line :{$errline}".PHP_EOL;
    file_put_contents('errorLogs.txt', $message,FILE_APPEND);
    if(DEBUG)
    {
        echo $message;
    }
    else
    {
        header("Location:/error/500");
    }
    
}
function exceptionHandler(Exception $exception)
{
    $message = date('Y.m.d \i\n H:i:s').$exception->getMessage().'Stack Tracing:'.$exception->getTraceAsString().PHP_EOL;
    file_put_contents('exceptionLogs.txt', $message,FILE_APPEND);
    if(DEBUG)
    {
        echo $message;
    }
    else
    {
        header("Location:/error/500");
    }
}
function dump($data)
{
    echo '<pre>',  var_export($data,true), '</pre>';
    exit();
}

set_error_handler('errorHandler');
set_exception_handler('exceptionHandler');
require_once CORE.'Application.php';
Application::run();
